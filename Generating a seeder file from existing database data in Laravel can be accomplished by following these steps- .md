Generating a seeder file from existing database data in Laravel can be accomplished by following these steps:

1. **Create the Seeder File:**
   Use the Artisan command to generate a seeder file. This file will later be populated with the data you retrieve from your database.

   ```bash
   php artisan make:seeder YourTableSeeder
   ```

2. **Fetch Data from the Database:**
   Retrieve the data from the database table you want to create a seeder for. This can be done within a custom Artisan command or directly in the seeder itself.

3. **Populate the Seeder File:**
   Insert the data into the newly created seeder file. You will manually format this data into a structure that can be used by Laravel's seeder system.

Here’s a step-by-step guide:

### Step 1: Create a Seeder File

Run the following Artisan command to create a seeder file for your table (replace `YourTable` with your actual table name):

```bash
php artisan make:seeder YourTableSeeder
```

This will create a new seeder file in the `database/seeders` directory.

### Step 2: Fetch Data from the Database

You can use Laravel's Eloquent or the DB facade to fetch data. For simplicity, we'll use the DB facade here. You can do this directly in the seeder or create a custom Artisan command to handle the data fetching and conversion.

### Step 3: Populate the Seeder File

Open the newly created seeder file in `database/seeders/YourTableSeeder.php`. Modify it to include the data fetched from your database.

Here's an example process:

#### Fetch and Generate Seeder Content

You can use a custom Artisan command or a script to fetch the data from your table and format it as a seeder file.

1. **Create a Custom Command:**

   ```bash
   php artisan make:command GenerateSeeder
   ```

   This will create a new command file in `app/Console/Commands/GenerateSeeder.php`.

2. **Modify the Command to Fetch Data:**

   Open `app/Console/Commands/GenerateSeeder.php` and modify the `handle` method to fetch data from your table and generate a seeder.

   ```php
   <?php

   namespace App\Console\Commands;

   use Illuminate\Console\Command;
   use Illuminate\Support\Facades\DB;

   class GenerateSeeder extends Command
   {
       protected $signature = 'generate:seeder {table} {--class=YourTableSeeder}';
       protected $description = 'Generate a seeder from database table';

       public function handle()
       {
           $table = $this->argument('table');
           $className = $this->option('class');

           $data = DB::table($table)->get();

           $seederContent = "<?php\n\n";
           $seederContent .= "use Illuminate\\Database\\Seeder;\n";
           $seederContent .= "use Illuminate\\Support\\Facades\\DB;\n";
           $seederContent .= "use Illuminate\\Support\\Facades\\Schema;\n\n";
           $seederContent .= "class {$className} extends Seeder\n";
           $seederContent .= "{\n";
           $seederContent .= "    public function run()\n";
           $seederContent .= "    {\n";
           $seederContent .= "        DB::table('$table')->insert([\n";

           foreach ($data as $row) {
               $seederContent .= "            [\n";
               foreach ((array) $row as $column => $value) {
                   $value = addslashes($value);
                   $seederContent .= "                '$column' => '$value',\n";
               }
               $seederContent .= "            ],\n";
           }

           $seederContent .= "        ]);\n";
           $seederContent .= "    }\n";
           $seederContent .= "}\n";

           file_put_contents(database_path("seeders/{$className}.php"), $seederContent);

           $this->info("Seeder generated for table: $table");
       }
   }
   ```

3. **Run the Custom Command:**

   Execute the command with your table name to generate the seeder:

   ```bash
   php artisan generate:seeder your_table
   ```

### Step 4: Use the Seeder

Finally, run the seeder using Artisan after you have dropped and recreated your table.

```bash
php artisan db:seed --class=YourTableSeeder
```

### Full Example

Here’s a complete example of how the `YourTableSeeder.php` might look after you generate it:

```php
<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class YourTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('your_table')->insert([
            [
                'column1' => 'value1',
                'column2' => 'value2',
                // more columns and values
            ],
            [
                'column1' => 'value3',
                'column2' => 'value4',
                // more columns and values
            ],
            // more rows
        ]);
    }
}
```

### Conclusion

By following these steps, you can create a seeder file from existing database data in Laravel without relying on external packages. This seeder can then be used to repopulate your database after dropping tables or making schema changes.