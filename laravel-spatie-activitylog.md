## Spatie Laravel Activity Log

### Install package
```
composer require spatie/laravel-activitylog
```

### Install migration
```
php artisan vendor:publish --provider="Spatie\Activitylog\ActivitylogServiceProvider" --tag="activitylog-migrations"

php artisan migrate
```

### Publish configuration
```
php artisan vendor:publish --provider="Spatie\Activitylog\ActivitylogServiceProvider" --tag="activitylog-config"
```

### Create cache. *&mdash; Not needed for local development*
```
php artisan config:cache
```

### Add LogsActivity trait into your model that you want to trace activity logs
```
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class YourModel extends Model {
    use LogsActivity;

    public function getActivitylogOptions(): LogOptions {
        return LogOptions::defaults()->logAll();
    }
}
```