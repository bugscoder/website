Automating the setup steps for the Spatie Laravel Permission package in future projects can save you time and ensure consistency. You can achieve this through a combination of Laravel Artisan commands, custom scripts, and possibly a seeder to initialize roles and permissions. Here's a step-by-step guide to automate the setup:

### 1. **Create a Custom Artisan Command**

You can create a custom Artisan command to automate the setup process for the Spatie Laravel Permission package.

1. **Generate the Command:**

    ```bash
    php artisan make:command SetupSpatiePermission
    ```

2. **Edit the Command:**

    Open `app/Console/Commands/SetupSpatiePermission.php` and modify the `handle` method to include the necessary steps.

    ```php
    <?php

    namespace App\Console\Commands;

    use Illuminate\Console\Command;
    use Illuminate\Support\Facades\Artisan;
    use Illuminate\Support\Facades\Schema;
    use Spatie\Permission\Models\Role;
    use Spatie\Permission\Models\Permission;

    class SetupSpatiePermission extends Command
    {
        protected $signature = 'setup:spatie-permission';
        protected $description = 'Automate Spatie Laravel Permission package setup';

        public function handle()
        {
            // Publish the migration and config
            $this->info('Publishing Spatie Permission files...');
            Artisan::call('vendor:publish', [
                '--provider' => 'Spatie\Permission\PermissionServiceProvider',
                '--tag' => 'config'
            ]);
            Artisan::call('vendor:publish', [
                '--provider' => 'Spatie\Permission\PermissionServiceProvider',
                '--tag' => 'migrations'
            ]);
            
            // Run the migration
            $this->info('Running migrations...');
            Artisan::call('migrate');

            // Initialize roles and permissions
            $this->info('Creating default roles and permissions...');
            $this->initializeRolesAndPermissions();

            $this->info('Spatie Laravel Permission setup complete.');
        }

        private function initializeRolesAndPermissions()
        {
            // Add your default roles
            $roles = ['admin', 'user', 'guest'];
            foreach ($roles as $role) {
                if (!Role::where('name', $role)->exists()) {
                    Role::create(['name' => $role]);
                }
            }

            // Add your default permissions
            $permissions = ['edit articles', 'delete articles', 'publish articles'];
            foreach ($permissions as $permission) {
                if (!Permission::where('name', $permission)->exists()) {
                    Permission::create(['name' => $permission]);
                }
            }

            // Assign permissions to roles (Example)
            $admin = Role::findByName('admin');
            $admin->givePermissionTo($permissions);

            $user = Role::findByName('user');
            $user->givePermissionTo(['edit articles']);
        }
    }
    ```

### 2. **Create a Seeder for Roles and Permissions**

Create a seeder to insert initial roles and permissions.

1. **Generate the Seeder:**

    ```bash
    php artisan make:seeder RolesAndPermissionsSeeder
    ```

2. **Edit the Seeder:**

    Open `database/seeders/RolesAndPermissionsSeeder.php` and modify it:

    ```php
    <?php

    namespace Database\Seeders;

    use Illuminate\Database\Seeder;
    use Spatie\Permission\Models\Role;
    use Spatie\Permission\Models\Permission;

    class RolesAndPermissionsSeeder extends Seeder
    {
        public function run()
        {
            // Define roles
            $roles = ['admin', 'user', 'guest'];

            // Define permissions
            $permissions = ['edit articles', 'delete articles', 'publish articles'];

            // Create roles
            foreach ($roles as $role) {
                Role::firstOrCreate(['name' => $role]);
            }

            // Create permissions
            foreach ($permissions as $permission) {
                Permission::firstOrCreate(['name' => $permission]);
            }

            // Assign permissions to roles (Example)
            $admin = Role::findByName('admin');
            $admin->syncPermissions($permissions);

            $user = Role::findByName('user');
            $user->syncPermissions(['edit articles']);
        }
    }
    ```

### 3. **Create a Setup Script**

You can create a custom script to handle the setup and call this script after a fresh install.

1. **Create the Script File:**

    Create a file, e.g., `setup_spatie_permission.sh`.

    ```bash
    touch setup_spatie_permission.sh
    chmod +x setup_spatie_permission.sh
    ```

2. **Edit the Script:**

    Open `setup_spatie_permission.sh` and add the following:

    ```bash
    #!/bin/bash

    echo "Publishing Spatie Permission files..."
    php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider" --tag="config"
    php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider" --tag="migrations"

    echo "Running migrations..."
    php artisan migrate

    echo "Running Roles and Permissions Seeder..."
    php artisan db:seed --class=RolesAndPermissionsSeeder

    echo "Spatie Laravel Permission setup complete."
    ```

### 4. **Automate the Process**

Add the script execution in your deployment process or as a part of the initial setup process.

1. **Add to `composer.json`:**

    You can add a script in your `composer.json` to run this setup script after installation.

    ```json
    "scripts": {
        "post-install-cmd": [
            "./setup_spatie_permission.sh"
        ]
    }
    ```

2. **Manual Execution:**

    After cloning a fresh project and running `composer install`, you can execute the script manually:

    ```bash
    ./setup_spatie_permission.sh
    ```

### 5. **Update `.env` for Fresh Install**

Ensure your `.env` file has the correct database settings before running the setup script. You might add a check in your script to ensure the database connection is configured.

### Conclusion

By following these steps, you can automate the installation and configuration of the Spatie Laravel Permission package, streamlining your project setup process for future applications. This approach ensures that roles and permissions are consistently initialized across all your projects.