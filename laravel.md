## Laravel

### Install Laravel

```
composer create-project laravel/laravel <project name>
```

### Change Directory

```
cd <project name>
```

### Install Laravel Breeze

```
composer require laravel/breeze --dev
php artisan breeze:install vue
```

### Start Vite development server

To enable instant hot-module replacement while building application

```
npm run dev
```

### Start Laravel local development server

```
php artisan serve
```