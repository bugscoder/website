## Jetstream

### Installing Jetstream

> Jetstream should only be installed into new Laravel applications.

```
composer require laravel/jetstream

php artisan jetstream:install <stack> [--teams] [--ssr] [--dark]
```

| | |
| - | - |
| stack | livewire / inertia |
| --teams | enable teams support |
| --ssr | enable SSR support |
| --dark | include frontend "dark mode" |

### Finalizing installation

```
npm install

npm run build

php artisan migrate
```