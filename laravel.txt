git clone <git link>
composer install
sudo npm install
sudo npm run build

change database config
	config > database.php

php artisan migrate
	sqlite location: database > database.sqlite

sudo nano .env
	APP_KEY= 
sudo php artisan key:generate

php artisan serve

===

===
install
===
composer create-project laravel/laravel chirper
cd chirper
composer require laravel/breeze --dev
php artisan breeze:install vue
npm run dev
php artisan serve

===
create / display
===
php artisan make:model -mrc Chirp
	app/Models/Chirp.php
	database/migrations/<timestamp>_create_chirps_table.php
	app/Http/Controllers/ChirpController.php

route
---
index - display/list
store - save

routes/web.php
	use App\Http\Controllers\ChirpController;

	Route::resource('chirps', ChirpController::class)
    	->only(['index', 'store'])
    	->middleware(['auth', 'verified']);
    GET 	chirps.index
    POST 	chirps.store

app/Http/Controllers/ChirpController.php
	//use Illuminate\Http\Response;
	use Inertia\Inertia;
	use Inertia\Response;

	public function index(): Response {
		//return response('Hello, World!');
		return Inertia::render('Chirps/Index', [
            //
        ]);
	}

resources/js/Pages/Chirps/Index.vue
	<script setup>
	import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout.vue';
	import InputError from '@/Components/InputError.vue';
	import PrimaryButton from '@/Components/PrimaryButton.vue';
	import { useForm, Head } from '@inertiajs/vue3';
	 
	const form = useForm({
	    message: '',
	});
	</script>
	 
	<template>
	    <Head title="Chirps" />
	 
	    <AuthenticatedLayout>
	        <div class="max-w-2xl mx-auto p-4 sm:p-6 lg:p-8">
	            <form @submit.prevent="form.post(route('chirps.store'), { onSuccess: () => form.reset() })">
	                <textarea
	                    v-model="form.message"
	                    placeholder="What's on your mind?"
	                    class="block w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm"
	                ></textarea>
	                <InputError :message="form.errors.message" class="mt-2" />
	                <PrimaryButton class="mt-4">Chirp</PrimaryButton>
	            </form>
	        </div>
	    </AuthenticatedLayout>
	</template>

resources/js/Layouts/AuthenticatedLayout.vue
	<NavLink :href="route('chirps.index')" :active="route().current('chirps.index')">
        Chirps
    </NavLink>

save
---
app/Http/Controllers/ChirpController.php
	use Illuminate\Http\RedirectResponse;

	public function store(Request $request): RedirectResponse
    {
    	$validated = $request->validate([
            'message' => 'required|string|max:255',
        ]);
 
        $request->user()->chirps()->create($validated);
 
        return redirect(route('chirps.index'));
    }

app/Models/User.php
	use Illuminate\Database\Eloquent\Relations\HasMany;

	public function chirps(): HasMany
    {
        return $this->hasMany(Chirp::class);
    }

app/Models/Chirp.php
	protected $fillable = [
        'message',
    ];

database/migrations/<timestamp>_create_chirps_table.php
	Schema::create('chirps', function (Blueprint $table) {
        $table->id();
        $table->foreignId('user_id')->constrained()->cascadeOnDelete();
        $table->string('message');
        $table->timestamps();
    });

php artisan migrate


db inspection
---
php artisan db:show
php artisan db:table users

php artisan tinker
App\Models\Chirp::all();

===
read / show
===
retrieve
---
app/Http/Controllers/ChirpController.php
	public function index(): Response
    {
        return Inertia::render('Chirps/Index', [
            'chirps' => Chirp::with('user:id,name')->latest()->get(),
        ]);
    }

retrieve belong to user only
---
app/Models/Chirp.php
	use Illuminate\Database\Eloquent\Relations\BelongsTo;

	public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

resources/js/Components/Chirp.vue
	<script setup>
	defineProps(['chirp']);
	</script>
	 
	<template>
	    <div class="p-6 flex space-x-2">
	        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-gray-600 -scale-x-100" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
	            <path stroke-linecap="round" stroke-linejoin="round" d="M8 12h.01M12 12h.01M16 12h.01M21 12c0 4.418-4.03 8-9 8a9.863 9.863 0 01-4.255-.949L3 20l1.395-3.72C3.512 15.042 3 13.574 3 12c0-4.418 4.03-8 9-8s9 3.582 9 8z" />
	        </svg>
	        <div class="flex-1">
	            <div class="flex justify-between items-center">
	                <div>
	                    <span class="text-gray-800">{{ chirp.user.name }}</span>
	                    <small class="ml-2 text-sm text-gray-600">{{ new Date(chirp.created_at).toLocaleString() }}</small>
	                </div>
	            </div>
	            <p class="mt-4 text-lg text-gray-900">{{ chirp.message }}</p>
	        </div>
	    </div>
	</template>

resources/js/Pages/Chirps/Index.vue
	import Chirp from '@/Components/Chirp.vue';

	defineProps(['chirps']);

	<div class="mt-6 bg-white shadow-sm rounded-lg divide-y">
        <Chirp
            v-for="chirp in chirps"
            :key="chirp.id"
            :chirp="chirp"
        />
    </div>