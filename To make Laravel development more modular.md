To make Laravel development more modular with Inertia.js and Vue, we'll follow a similar structure to the modular approach previously described, but we'll integrate Inertia.js for seamless single-page application (SPA) functionality. This approach allows you to create self-contained components or modules that can be easily reused and managed within your Laravel applications.

### Step-by-Step Guide to Modular Programming with Inertia.js and Vue

1. **Set Up a New Laravel Project**:
   - First, set up a new Laravel project and install the necessary dependencies for Inertia.js and Vue.

2. **Install Inertia.js and Vue**:
   - Install Inertia.js and Vue in your Laravel project.

### Example: Creating a Modular Component with Inertia.js and Vue

#### Step 1: Create a New Laravel Project

```bash
composer create-project --prefer-dist laravel/laravel inertia-modular-app
cd inertia-modular-app
```

#### Step 2: Install Inertia.js and Vue

```bash
composer require inertiajs/inertia-laravel
npm install @inertiajs/inertia @inertiajs/inertia-vue3
npm install vue@next
```

#### Step 3: Set Up Inertia.js

1. **Set up Inertia middleware in Laravel**:
   - In `app/Http/Kernel.php`, add the Inertia middleware to the web group:

     ```php
     use Inertia\Middleware;

     protected $middlewareGroups = [
         'web' => [
             // Other middleware...
             \App\Http\Middleware\HandleInertiaRequests::class,
         ],
     ];
     ```

2. **Create an Inertia middleware**:
   - Run the following command to create the Inertia middleware:

     ```bash
     php artisan inertia:middleware
     ```

   - This will create `app/Http/Middleware/HandleInertiaRequests.php`.

3. **Update the Inertia middleware** (optional for customizations).

4. **Configure your Vite setup for Vue**:
   - In your `vite.config.js`, set up the Vue plugin:

     ```javascript
     import { defineConfig } from 'vite';
     import vue from '@vitejs/plugin-vue';

     export default defineConfig({
         plugins: [vue()],
         resolve: {
             alias: {
                 '@': '/resources/js',
             },
         },
     });
     ```

5. **Set up the `app.js` file for Inertia and Vue**:
   - In `resources/js/app.js`:

     ```javascript
     import { createApp, h } from 'vue';
     import { createInertiaApp } from '@inertiajs/inertia-vue3';

     createInertiaApp({
         resolve: name => import(`./Pages/${name}`),
         setup({ el, App, props, plugin }) {
             createApp({ render: () => h(App, props) })
                 .use(plugin)
                 .mount(el);
         },
     });
     ```

6. **Set up the Blade view for Inertia**:
   - Create `resources/views/app.blade.php`:

     ```html
     <!DOCTYPE html>
     <html lang="en">
     <head>
         <meta charset="UTF-8">
         <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <title>Inertia App</title>
         @vite('resources/js/app.js')
     </head>
     <body>
         @inertia
     </body>
     </html>
     ```

#### Step 4: Create a Modular Component

1. **Create a New Module Directory**:
   - Create a directory for your custom component or module, for example, `resources/js/Pages/CustomComponent`.

2. **Create Vue Components**:
   - In `resources/js/Pages/CustomComponent`, create a new file named `Form.vue`:

     ```vue
     <template>
         <div>
             <h1>Create Item</h1>
             <form @submit.prevent="submit">
                 <label for="name">Name:</label>
                 <input type="text" v-model="form.name">
                 <button type="submit">Submit</button>
             </form>
         </div>
     </template>

     <script setup>
     import { reactive } from 'vue';
     import { useForm } from '@inertiajs/inertia-vue3';

     const form = reactive({
         name: '',
     });

     const submit = () => {
         // Replace 'customcomponent.store' with the route name
         useForm(form).post(route('customcomponent.store'), {
             onSuccess: () => {
                 form.name = '';
                 // Optional: Redirect to another page or show a success message
             },
         });
     };
     </script>
     ```

3. **Create Laravel Controller and Route**:

   - **Create a Controller**:
     ```bash
     php artisan make:controller CustomComponentController
     ```

     - In `app/Http/Controllers/CustomComponentController.php`:

       ```php
       namespace App\Http\Controllers;

       use Illuminate\Http\Request;
       use Inertia\Inertia;

       class CustomComponentController extends Controller
       {
           public function create()
           {
               return Inertia::render('CustomComponent/Form');
           }

           public function store(Request $request)
           {
               $request->validate(['name' => 'required']);

               // Save the item to the database
               // Example: Item::create($request->all());

               return redirect()->route('customcomponent.edit', $item->id)
                                ->with('success', 'Item created successfully!');
           }

           public function edit($id)
           {
               // Fetch the item from the database
               // Example: $item = Item::findOrFail($id);

               return Inertia::render('CustomComponent/Form', [
                   'item' => $item,
               ]);
           }
       }
       ```

   - **Define Routes**:
     - In `routes/web.php`:

       ```php
       use App\Http\Controllers\CustomComponentController;

       Route::get('customcomponent/create', [CustomComponentController::class, 'create'])->name('customcomponent.create');
       Route::post('customcomponent/store', [CustomComponentController::class, 'store'])->name('customcomponent.store');
       Route::get('customcomponent/{id}/edit', [CustomComponentController::class, 'edit'])->name('customcomponent.edit');
       ```

#### Step 5: Handle Redirect and Data Loading

1. **Redirect After Submission**:
   - After successfully submitting the form, redirect to the edit page of the newly created item with a thank you message.

2. **Load Data in Edit Form**:
   - Modify the `Form.vue` to handle data loading and pre-fill the form fields if editing an existing item:

     ```vue
     <template>
         <div>
             <h1>{{ item ? 'Edit' : 'Create' }} Item</h1>
             <form @submit.prevent="submit">
                 <label for="name">Name:</label>
                 <input type="text" v-model="form.name">
                 <button type="submit">{{ item ? 'Update' : 'Create' }}</button>
             </form>
             <div v-if="message" class="mt-2 p-2 text-green-500 bg-green-100 border border-green-400">
                 {{ message }}
             </div>
         </div>
     </template>

     <script setup>
     import { reactive } from 'vue';
     import { useForm } from '@inertiajs/inertia-vue3';
     import { usePage } from '@inertiajs/inertia-vue3';

     const props = defineProps({
         item: Object,
     });

     const { props: pageProps } = usePage();

     const form = reactive({
         name: props.item ? props.item.name : '',
     });

     const submit = () => {
         useForm(form).post(route(props.item ? 'customcomponent.update' : 'customcomponent.store', props.item?.id), {
             onSuccess: () => {
                 form.name = props.item ? form.name : '';
                 // Optional: Redirect to another page or show a success message
             },
         });
     };

     const message = pageProps.flash?.success || '';
     </script>
     ```

3. **Update the Controller**:
   - In `CustomComponentController`, update the store method to redirect to the edit page with a session flash message.

     ```php
     public function store(Request $request)
     {
         $request->validate(['name' => 'required']);

         // Save the item to the database
         $item = Item::create($request->all());

         return redirect()->route('customcomponent.edit', $item->id)
                          ->with('success', 'Item created successfully!');
     }
     ```

4. **Handle Sessions in Middleware**:
   - Ensure your Inertia middleware is configured to handle session flash data.

   ```php
   protected function shared(Request $request)
   {
       return array_merge(parent::shared($request), [
           'flash' => [
               'success' => session('success'),
           ],
       ]);
   }
   ```

#### Step 6: Test and Refine

- Test the module by accessing `http://yourapp.test/customcomponent/create` to create a new item, and after submission, ensure it redirects to the edit page with the form pre-filled and a success message displayed.

### Resources for Further Study

1. **Inertia.js Documentation**:
   - [Inertia.js Documentation](https://inertiajs.com/)

2. **Vue 3 Documentation**:
   - [Vue 3 Documentation](https://v3.vuejs.org/)

3. **Laracasts on Inertia.js**:
   - [Laracasts: Building a SPA with Inertia.js](https://laracasts.com/series/building-modern-laravel-apps)

4. **Example Projects**:
   - [Inertia.js Example Projects on GitHub](https://github.com/inertiajs/inertia-laravel)

By using this approach, you can create modular and reusable components in Laravel with Inertia.js and Vue, making your development process more organized and efficient.