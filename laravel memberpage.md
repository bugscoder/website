Certainly! Let's update the CRUD example for a User model instead of a Member model, maintaining the use of Spatie's permission package and leveraging Laravel, InertiaJS, and Vue for a modern user management system.

### Step-by-Step Guide for User Management

#### 1. **Set Up Laravel and Spatie Permission Package**

1. **Install Laravel**:
   ```bash
   composer create-project laravel/laravel user-management
   cd user-management
   ```

2. **Install Spatie Laravel Permission**:
   ```bash
   composer require spatie/laravel-permission
   ```

3. **Publish Spatie Configuration**:
   ```bash
   php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider"
   ```

4. **Run Migrations**:
   ```bash
   php artisan migrate
   ```

#### 2. **Set Up Models and Permissions**

1. **User Model**: Laravel comes with a User model by default located at `app/Models/User.php`.

2. **Set Up Permissions** in `AppServiceProvider` or a custom seeder:
   ```php
   use Spatie\Permission\Models\Role;
   use Spatie\Permission\Models\Permission;

   public function boot()
   {
       Role::create(['name' => 'admin']);
       Role::create(['name' => 'user']);

       Permission::create(['name' => 'manage users']);
   }
   ```

#### 3. **Create Request Class for Validation**

1. **Generate Request Class**:
   ```bash
   php artisan make:request UserRequest
   ```

2. **Define Validation Rules** in `app/Http/Requests/UserRequest.php`:
   ```php
   namespace App\Http\Requests;

   use Illuminate\Foundation\Http\FormRequest;

   class UserRequest extends FormRequest
   {
       public function authorize()
       {
           return true;
       }

       public function rules()
       {
           return [
               'name' => 'required|string|max:255',
               'email' => 'required|email|unique:users,email,' . $this->route('user'),
               'password' => 'nullable|string|min:8|confirmed',
           ];
       }

       public function messages()
       {
           return [
               'name.required' => 'The name field is required.',
               'email.required' => 'The email field is required.',
               'email.unique' => 'This email is already taken.',
               'password.min' => 'The password must be at least 8 characters.',
               'password.confirmed' => 'The password confirmation does not match.',
           ];
       }
   }
   ```

#### 4. **Create Controller and Routes**

1. **Generate Controller**:
   ```bash
   php artisan make:controller UserController
   ```

2. **Define CRUD Methods** in `app/Http/Controllers/UserController.php`:
   ```php
   namespace App\Http\Controllers;

   use App\Http\Requests\UserRequest;
   use App\Models\User;
   use Illuminate\Support\Facades\Hash;
   use Inertia\Inertia;
   use Illuminate\Http\Request;

   class UserController extends Controller
   {
       public function index()
       {
           $users = User::all();
           return Inertia::render('Users/Index', compact('users'));
       }

       public function create()
       {
           return Inertia::render('Users/Form', ['user' => new User()]);
       }

       public function edit(User $user)
       {
           return Inertia::render('Users/Form', compact('user'));
       }

       public function save(UserRequest $request, User $user = null)
       {
           $data = $request->validated();

           if ($user) {
               // Update existing user
               $user->update([
                   'name' => $data['name'],
                   'email' => $data['email'],
                   'password' => $data['password'] ? Hash::make($data['password']) : $user->password,
               ]);
               $message = 'User updated successfully.';
           } else {
               // Create new user
               $user = User::create([
                   'name' => $data['name'],
                   'email' => $data['email'],
                   'password' => Hash::make($data['password']),
               ]);
               $message = 'User created successfully.';
           }

           return redirect()->route('users.index')->with('success', $message);
       }

       public function destroy(User $user)
       {
           $user->delete();
           return redirect()->route('users.index')->with('success', 'User deactivated successfully.');
       }
   }
   ```

3. **Define Routes** in `routes/web.php`:
   ```php
   use App\Http\Controllers\UserController;

   Route::middleware(['auth', 'role:admin'])->group(function () {
       Route::get('users', [UserController::class, 'index'])->name('users.index');
       Route::get('users/create', [UserController::class, 'create'])->name('users.create');
       Route::post('users', [UserController::class, 'save'])->name('users.store');
       Route::get('users/{user}/edit', [UserController::class, 'edit'])->name('users.edit');
       Route::put('users/{user}', [UserController::class, 'save'])->name('users.update');
       Route::delete('users/{user}', [UserController::class, 'destroy'])->name('users.destroy');
   });
   ```

#### 5. **Create Vue Components for InertiaJS**

1. **Install Inertia and Vue**:
   ```bash
   npm install @inertiajs/inertia @inertiajs/inertia-vue3
   npm install vue@next
   ```

2. **Configure Inertia and Vue** in `resources/js/app.js`:
   ```javascript
   import { createApp, h } from 'vue';
   import { createInertiaApp } from '@inertiajs/inertia-vue3';
   import { InertiaProgress } from '@inertiajs/progress';

   createInertiaApp({
       resolve: name => require(`./Pages/${name}`),
       setup({ el, App, props, plugin }) {
           createApp({ render: () => h(App, props) })
               .use(plugin)
               .mount(el);
       },
   });

   InertiaProgress.init();
   ```

3. **Create User Components**:

    - **Index.vue** in `resources/js/Pages/Users/Index.vue`:
      ```vue
      <template>
          <div>
              <h1 class="text-xl">Users List</h1>
              <table class="table-auto w-full">
                  <thead>
                      <tr>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Actions</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr v-for="user in users" :key="user.id">
                          <td>{{ user.name }}</td>
                          <td>{{ user.email }}</td>
                          <td>
                              <inertia-link :href="route('users.edit', user.id)">Edit</inertia-link>
                              <form :action="route('users.destroy', user.id)" method="POST">
                                  <input type="hidden" name="_method" value="DELETE">
                                  <button type="submit">Delete</button>
                              </form>
                          </td>
                      </tr>
                  </tbody>
              </table>
          </div>
      </template>

      <script setup>
      import { ref } from 'vue';
      const props = defineProps();
      const users = ref(props.users);
      </script>
      ```

    - **Form.vue** in `resources/js/Pages/Users/Form.vue`:
      ```vue
      <template>
          <form @submit.prevent="handleSubmit">
              <div>
                  <label for="name">Name</label>
                  <input type="text" id="name" v-model="form.name">
                  <span v-if="errors.name">{{ errors.name[0] }}</span>
              </div>
              <div>
                  <label for="email">Email</label>
                  <input type="email" id="email" v-model="form.email">
                  <span v-if="errors.email">{{ errors.email[0] }}</span>
              </div>
              <div>
                  <label for="password">Password</label>
                  <input type="password" id="password" v-model="form.password">
                  <span v-if="errors.password">{{ errors.password[0] }}</span>
              </div>
              <div>
                  <label for="password_confirmation">Confirm Password</label>
                  <input type="password" id="password_confirmation" v-model="form.password_confirmation">
              </div>
              <button type="submit">Save</button>
          </form>
      </template>

      <script setup>
      import { ref } from 'vue';
      import { useForm } from '@inertiajs/inertia-vue3';

      const props = defineProps();
      const form = useForm({
          name: props.user?.name || '',
          email: props.user?.email || '',
          password: '',
          password_confirmation: ''
      });

      const handleSubmit = () => {
          if (props.user.id) {
              form.put(route('users.update', props.user.id));
          } else {
              form.post(route('users.store'));
          }
      };
      </script>
      ```

#### 6. **Enable Soft Deletes**

1. **Add Soft Deletes to User Model**:

    - Add `SoftDeletes` trait to `User` model in `app/Models/User.php`:
      ```php
      namespace App\Models;

      use Illuminate\Database\Eloquent\Factories\HasFactory;
      use Illuminate\Database\Eloquent\SoftDeletes;
      use Illuminate\Foundation\Auth\User as Authenticatable;

      class User extends Authenticatable
      {
          use HasFactory, SoftDeletes;

          protected $fillable = [
              'name',
              'email',
              'password',
          ];
      }
      ```

2. **Update Migration**:

    - Add `softDeletes` to the migration:
      ```php
      public function up()
      {
          Schema::create('users', function (Blueprint $table) {
              $table->id();
              $table->string('name');
              $table->string('email')->unique();
              $table->string('password');
              $table->timestamps();
              $table->softDeletes();
          });
      }
      ```

    - Run the migration again if necessary:
      ```bash
      php artisan migrate
      ```

3. **Modify Controller**:

    - Update the `destroy` method in the controller to use soft deletes:
      ```php
      public function destroy(User $user)
      {
          $user->delete();
          return redirect()->route('users.index')->with('success', 'User deactivated successfully.');
      }
      ```

### Conclusion

This setup allows you to manage users effectively using a single form for both creating and updating records, leveraging Spatie's permission package. The use of InertiaJS and Vue ensures a modern, reactive user interface, while soft deletes provide a way to safely remove users without permanently deleting them from the database. Validation is handled separately via a request class, keeping the controller clean and focused on logic.