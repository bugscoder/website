reference:
https://www.youtube.com/watch?v=jwGYOYeNKsA
https://www.youtube.com/watch?v=SeEQKmYQxRc

///

function AddNewInventory() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var shtAD = ss.getSheetByName("Add & Deploy");
  var shtTrans = ss.getSheetByName("Transaction History");
  var d = new Date();
  var rngToAdd = shtAD.getRange("D2:D6").getValues();
  var horizRng = rngToAdd.flat();
  horizRng.push(0, horizRng[4], d.toLocaleString());
  var finalRng = [horizRng];
  var lastRow = shtTrans.getLastRow() + 1;
  shtTrans.getRange("A" + lastRow + ":H" + lastRow).setValues(finalRng);
  shtAD.getRange("D2").clearContent();
  shtAD.getRange("D3:D9").clearContent();
}

function AddNewInventoryForm(description, purchaseDate, unitCost, quantity) {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var shtTrans = ss.getSheetByName("Transaction History");
  var d = new Date();
  var finalRng = [description, purchaseDate, unitCost, quantity, (unitCost*quantity), d.toLocaleString()];
  var lastRow = shtTrans.getLastRow() + 1;
  shtTrans.getRange("A" + lastRow + ":F" + lastRow).setValues([finalRng]);
  //shtTrans.appendRow(finalRng);
}

function AddInventoryForm() { 
  var form = HtmlService.createHtmlOutputFromFile('addInventory').setTitle('Add Inventory');
  SpreadsheetApp.getUi().showSidebar(form);
}

function addMenu() {
  var menu = SpreadsheetApp.getUi().createMenu('Inventory Tracker');
  menu.addItem('Add Inventory', 'AddInventoryForm');
  menu.addToUi(); 
}

function onOpen(e) {
  addMenu();
}

///

addInventory.html
===
<!DOCTYPE html>
<html>
  <head>
    <base target="_top">

  <script>
  function SubmitRecord() {
    var description = document.getElementById("desc").value;
    var purchaseDate = document.getElementById("purchase_date").value;
    var unitCost = document.getElementById("unit_cost").value;
    var quantity = document.getElementById("quantity").value;
    google.script.run.withSuccessHandler(returnBack).AddNewInventoryForm(description, purchaseDate, unitCost, quantity);
  }

  function returnBack(stringBack) {
    document.getElementById("desc").value = '';
    document.getElementById("purchase_date").value = '';
    document.getElementById("unit_cost").value = '';
    document.getElementById("quantity").value = '';
  }
  </script>
  </head>
  <body>
    Inventory ID: <br>
    Description: <input type="text" name="desc" id="desc"><br><br>
    Purchase Date: <input type="date" name="purchase_date" id="purchase_date"><br><br>
    Cost Per Unit: <input type="text" name="unit_cost" id="unit_cost"><br><br>
    Quantity: <input type="text" name="quantity" id="quantity"><br><br>
    <input type="button" value="Add" onclick="SubmitRecord()">
  </body>
</html>
