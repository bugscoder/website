Making programming more modular in Laravel, especially in a way that mimics the plugin system in WordPress, involves creating self-contained modules or packages that can be easily integrated, activated, and managed within your Laravel application. This approach not only organizes your codebase but also makes it easier to maintain, extend, and reuse functionality across different projects.

Here's a step-by-step guide and examples to achieve modular programming in Laravel:

### Step-by-Step Guide to Modular Programming in Laravel

1. **Understand Laravel Packages and Modules**:
   - **Packages**: Laravel packages are the primary way to add functionality to your Laravel applications. They can include routes, controllers, views, and configuration specifically for Laravel.
   - **Modules**: Modules are similar to packages but are typically more integrated into your application. They can be thought of as mini-Laravel applications that encapsulate specific features or functionality.

2. **Set Up Laravel Package Development**:
   - Laravel has built-in support for creating packages, making it easier to modularize your application.

### Example: Creating a Modular Package in Laravel

#### Step 1: Create a New Laravel Project

```bash
composer create-project --prefer-dist laravel/laravel modular-app
cd modular-app
```

#### Step 2: Create a New Package Directory

1. **Create a `packages` directory** to store your custom packages:
   ```bash
   mkdir packages
   cd packages
   ```

2. **Create a new directory for your package**:
   ```bash
   mkdir CustomPlugin
   cd CustomPlugin
   ```

3. **Set up the basic package structure**:
   - Use the Laravel package skeleton to get started quickly:
     ```bash
     composer create-project --prefer-source laravel/package-skeleton my-package
     ```

   - Alternatively, create the structure manually:
     ```bash
     mkdir -p src/{Http/Controllers,Models,Views}
     touch src/{routes.php,ServiceProvider.php}
     ```

#### Step 3: Create a Service Provider

Create a service provider for your package to handle the registration of routes, views, and other resources.

- **`src/ServiceProvider.php`**:
  ```php
  <?php

  namespace CustomPlugin;

  use Illuminate\Support\ServiceProvider;

  class CustomPluginServiceProvider extends ServiceProvider {
      public function register() {
          // Register routes
          $this->loadRoutesFrom(__DIR__.'/routes.php');

          // Register views
          $this->loadViewsFrom(__DIR__.'/Views', 'customplugin');
      }

      public function boot() {
          // Publish configuration files, migrations, etc.
      }
  }
  ```

#### Step 4: Define Routes and Controllers

Define the routes for your package and create a controller to handle requests.

- **`src/routes.php`**:
  ```php
  <?php

  use Illuminate\Support\Facades\Route;
  use CustomPlugin\Http\Controllers\ExampleController;

  Route::group(['prefix' => 'customplugin', 'middleware' => ['web']], function () {
      Route::get('/', [ExampleController::class, 'index']);
  });
  ```

- **`src/Http/Controllers/ExampleController.php`**:
  ```php
  <?php

  namespace CustomPlugin\Http\Controllers;

  use App\Http\Controllers\Controller;

  class ExampleController extends Controller {
      public function index() {
          return view('customplugin::index');
      }
  }
  ```

#### Step 5: Create Views

Create the view files that will be used by your package.

- **`src/Views/index.blade.php`**:
  ```html
  <html>
  <head>
      <title>Custom Plugin</title>
  </head>
  <body>
      <h1>Welcome to Custom Plugin</h1>
  </body>
  </html>
  ```

#### Step 6: Register the Package

Register your package in the Laravel application so it can be recognized and used.

1. **Add the service provider to `config/app.php`**:
   ```php
   'providers' => [
       // Other Service Providers
       CustomPlugin\CustomPluginServiceProvider::class,
   ],
   ```

2. **Autoload the package using Composer**:
   - Add the package to the `autoload` section of `composer.json`:
     ```json
     "autoload": {
         "psr-4": {
             "App\\": "app/",
             "CustomPlugin\\": "packages/CustomPlugin/src"
         }
     }
     ```

3. **Update Composer autoload**:
   ```bash
   composer dump-autoload
   ```

### Example: Using `nWidart/laravel-modules` for Modular Development

An even more streamlined approach to modular development in Laravel is to use the `nWidart/laravel-modules` package. This package provides a structured way to build Laravel modules, making your application highly modular and easy to maintain.

#### Step 1: Install `nWidart/laravel-modules`

```bash
composer require nwidart/laravel-modules
```

#### Step 2: Publish Configuration

```bash
php artisan vendor:publish --provider="Nwidart\Modules\LaravelModulesServiceProvider"
```

#### Step 3: Create a Module

```bash
php artisan module:make CustomPlugin
```

#### Step 4: Define Module Components

- **Define Routes**: `Modules/CustomPlugin/Routes/web.php`
  ```php
  <?php

  Route::group(['middleware' => 'web', 'prefix' => 'customplugin'], function () {
      Route::get('/', 'CustomPlugin\Http\Controllers\CustomPluginController@index');
  });
  ```

- **Create Controller**: `Modules/CustomPlugin/Http/Controllers/CustomPluginController.php`
  ```php
  <?php

  namespace Modules\CustomPlugin\Http\Controllers;

  use Illuminate\Routing\Controller;

  class CustomPluginController extends Controller {
      public function index() {
          return view('customplugin::index');
      }
  }
  ```

- **Create Views**: `Modules/CustomPlugin/Resources/views/index.blade.php`
  ```html
  <html>
  <head>
      <title>Custom Plugin</title>
  </head>
  <body>
      <h1>Welcome to Custom Plugin Module</h1>
  </body>
  </html>
  ```

#### Step 5: Register the Module

Laravel Modules package automatically registers the modules, so no additional configuration is needed.

#### Step 6: Access the Module

Navigate to `http://yourapp.test/customplugin` to see your module in action.

### Resources for Further Study

1. **Laravel Packages**:
   - [Laravel Package Development](https://laravel.com/docs/11.x/packages)

2. **nWidart/laravel-modules Documentation**:
   - [nWidart Laravel Modules](https://nwidart.com/laravel-modules/v5/introduction)

3. **Laracasts on Package Development**:
   - [Laracasts: Package Development](https://laracasts.com/series/package-development)

4. **Blog Post on Modular Laravel**:
   - [Modular Laravel Applications](https://dev.to/nicolus/how-to-build-a-modular-application-in-laravel-53pe)

By following these steps, you can create modular, reusable components in Laravel that resemble the plugin system in WordPress. This approach helps keep your codebase clean, organized, and easy to maintain, enhancing the overall development experience.